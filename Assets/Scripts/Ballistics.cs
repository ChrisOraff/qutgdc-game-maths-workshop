﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ballistics : MonoBehaviour {

    public float moveSpeed = 15;

    public float angle = 45;
    public float gravity = 9.82f;
    public float velocity = 15;

    private float distance;
    private float height;

    private Vector3 origin;
    private Vector3 newPosition;

	public Transform model;

    //Audio
    public GameObject explosionSound;

    //Explosion Effect
    public GameObject explosionEffect;

    //Explosion Damage
    public GameObject explosionDamage;

    // Use this for initialization
    void Start () {

        Destroy(this.gameObject, 10);

        origin = transform.position;
        newPosition = origin;

        transform.GetComponent<AudioSource>().pitch = Random.Range(velocity/20, velocity/25);
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 oldPosition = transform.position;

        //Move forward
        transform.position += transform.forward * (velocity) * Time.deltaTime;


		/* ---------------------------------
		
			Ballistics code goes here		  

		--------------------------------- */


		// --------- Replace this line with the new 'newPosition = ' line
		newPosition = transform.position;
		// ---------

		if ((newPosition - oldPosition).sqrMagnitude > 0) {
			model.rotation = Quaternion.LookRotation(newPosition - oldPosition);
		}

        transform.position = newPosition;
	}


    private void OnTriggerEnter(Collider other) {
        
        if(other.transform.tag == "Ground") {
            GameObject thisSound = Instantiate(explosionSound, transform.position, transform.rotation) as GameObject;
            thisSound.GetComponent<AudioSource>().pitch = Random.Range(0.3f, 0.8f);
            Instantiate(explosionEffect, transform.position, transform.rotation);
            Instantiate(explosionDamage, transform.position, transform.rotation);
            Destroy(this.gameObject);
        }
    }
}
