﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Turret : MonoBehaviour {

    //Turret Variables and Aiming
	public GameObject turretObject;
	public GameObject target;
    private Quaternion targetRotation;
    public float rotationSpeed = 1.0f;

    //Projectile Firing
    public float turretFireSpeed = 0.2f;
    private float turretFireTime;
    public GameObject turretProjectile;
    public GameObject fireSound;
    public GameObject muzzle;
    public float firingAngle = 0;
    public float velocity = 25;
    public float gravity = 9.82f;

    //Projectile Path Render
    public LineRenderer projectilePath;
    private float maxDistance;
    public float timeResolution = 0.5f;
    public float maxTime = 10.0f;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

        TurretFiring();

        ProjectilePath();
	}


    void ProjectilePath() {
        
        //Render Path using time resolution and temporary Vector
        Vector3 velocityVector = muzzle.transform.forward * velocity;
        projectilePath.positionCount = (int)(maxTime/timeResolution);
        int index = 0;
        Vector3 currentPosition = muzzle.transform.position;

        for (float t = 0.0f; t < maxTime; t+= timeResolution) {

            projectilePath.SetPosition(index, currentPosition);
            currentPosition += velocityVector * timeResolution;
            velocityVector += Physics.gravity * timeResolution;
            index++;
        }
    }



    void TurretFiring(){

        // #### PROJECTILE VELOCITY
        if (Input.GetKeyDown("w")) {
            velocity += 5;
        } else if (Input.GetKeyDown("s")) {
            velocity -= 5;
        }
        velocity = Mathf.Clamp(velocity, 15, 30);

        // #### ROTATION ANGLE
        firingAngle = turretObject.transform.rotation.eulerAngles.x;
        if (firingAngle > 90)
            firingAngle = 360 - firingAngle;
        else
            firingAngle = -firingAngle;
        //Convert to Radians <<< very important!!!
        firingAngle = firingAngle * Mathf.Deg2Rad;

        //Slerp Rotate towards target - Smooth Lock
        //Determine the target rotation. This is the rotation if the transform looks at the target point
        targetRotation = Quaternion.LookRotation(target.transform.position - turretObject.transform.position);
        //Smoothly rotate towards the target point.
        turretObject.transform.rotation = Quaternion.Slerp(turretObject.transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);

        // #### FIRE PROJECTILE
        if (!GameManager.Paused && Input.GetMouseButton(0) && Time.time > turretFireTime) {

            //Projectile
            GameObject thisProjectile = Instantiate(turretProjectile, muzzle.transform.position, muzzle.transform.rotation) as GameObject;
            thisProjectile.GetComponent<Ballistics>().angle = firingAngle;
            thisProjectile.GetComponent<Ballistics>().velocity = velocity;

            //Audio
            Instantiate(fireSound, muzzle.transform.position, muzzle.transform.rotation);

            //Animation
            turretObject.GetComponent<Animation>().Play();

            //Cooldown timer
            turretFireTime = Time.time + turretFireSpeed;
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Enemy") {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
