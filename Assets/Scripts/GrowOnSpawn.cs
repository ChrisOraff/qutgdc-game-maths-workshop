﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrowOnSpawn : MonoBehaviour {

	public EasingFunctions.Function function;
	public float duration = 1;

	private Vector3 scale;
	private float timer;

	// Use this for initialization
	void Start () {
		scale = transform.localScale;
		transform.localScale = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update () {
		transform.localScale = scale;
	}
}
