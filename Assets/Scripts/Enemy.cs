﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour {

    NavMeshAgent agent;

    public GameObject target;

    public float health = 100.0f;

    public GameObject enemyDeath;

    public GameObject deathSound;

    // Use this for initialization
    void Start () {
        agent = GetComponent<NavMeshAgent>();

        target = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {

        agent.SetDestination(target.transform.position);
	}

    public void takeDamage(float damage) {

        health -= damage;

        if (health <= 0) {
            Destroy(this.gameObject);
            Instantiate(enemyDeath, transform.position, transform.rotation);
            Instantiate(deathSound, transform.position, transform.rotation);
        }
    }
}
