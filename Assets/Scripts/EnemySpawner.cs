﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    public float spawnInterval = 5.0f;
    private float timer;

    public GameObject enemy;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if(Time.time > timer) {
            Instantiate(enemy, transform.position, transform.rotation);
            timer = Time.time + spawnInterval;
        }
	}
}
