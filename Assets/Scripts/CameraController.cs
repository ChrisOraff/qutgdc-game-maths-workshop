﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public Transform cameraTarget;
	public float speed;
	public EasingFunctions.Function function;

	private ThirdPersonCamera thirdPersonCam;
	private Vector3 thirdPersonPos;
	private Quaternion thirdPersonRot;
	private float moveDist;

	private bool unpausing = false;
	private bool atPausePos = false;

	private void Start() {
		thirdPersonCam = GetComponent<ThirdPersonCamera>();
	}

	// Update is called once per frame
	private void Update () {
		if (Input.GetButtonDown("Cancel")) {
			if (GameManager.Paused && atPausePos) {
				// Only unpause when the camera is at the pause location
				atPausePos = false;
				unpausing = true;
				moveDist = 0;
			}
			else if (!GameManager.Paused) {				
				moveDist = 0;
				GameManager.Paused = true;
			}
		}

		if (!GameManager.Paused) {
			// Set the location and rotation to move back to when unpausing
			thirdPersonPos = transform.position;
			thirdPersonRot = transform.rotation;

			// Rotate the pause location based on the current view direction
			Vector3 forwardVec = transform.forward;
			forwardVec.y = 0;
			cameraTarget.rotation = Quaternion.LookRotation(Vector3.down, forwardVec);
		}
		else {
			Move();
			if (unpausing && Vector3.Distance(transform.position, thirdPersonPos) < 0.01f) {
				// The camera is back at the turret, unpause the game
				unpausing = false;
				GameManager.Paused = false;
			}
			if (GameManager.Paused && !unpausing) {
				// Check if the camera has reached the pause position
				atPausePos = Vector3.Distance(transform.position, cameraTarget.position) < 0.01f;
			}
		}

		GameManager.CanUnpause = atPausePos;

		// Stop the movement of the game when paused, and take access of the camera from the ThirdPersonController
		thirdPersonCam.enabled = !GameManager.Paused;
		Time.timeScale = GameManager.Paused ? 0 : 1;
	}

	private void Move() {
		// Set the start and end points based on the current target
		Vector3 startPos = unpausing ? cameraTarget.position : thirdPersonPos;
		Vector3 endPos = unpausing ? thirdPersonPos : cameraTarget.position;

		// Set the start and end rotation based on the current target
		Quaternion startRot = unpausing ? cameraTarget.rotation : thirdPersonRot;
		Quaternion endRot = unpausing ? thirdPersonRot : cameraTarget.rotation;

		// Note use UnscaledDeltaTime to counteract the pausing

		/* ---------------------------------
		
				Easing code goes here

		--------------------------------- */

		// Replace these with the slerp/lerp functions
		transform.position = endPos;
		transform.rotation = endRot;
		// -------------------------------------------
	}


	public void Unpause() {
		if (GameManager.Paused && atPausePos) {
			atPausePos = false;
			unpausing = true;
			moveDist = 0;
		}
	}
}
