﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public static bool Paused { get; set; }
	public static bool CanUnpause { get; set; }


	private void Update() {
		Cursor.lockState = CanUnpause ? CursorLockMode.None : CursorLockMode.Locked;
		Cursor.visible = CanUnpause;
	}
}
