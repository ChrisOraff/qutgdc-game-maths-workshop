﻿using UnityEngine;
using UnityEngine.UI;

public class PauseUIController : MonoBehaviour {

	private Button unpauseBtn;

	public Transform start;
	public Transform pausePos;
	public Transform end;

	public EasingFunctions.Function enterFunction;
	public EasingFunctions.Function exitFunction;

	public float duration = 1;

	private float timer;

	private bool prevUnpause;


	// Use this for initialization
	void Start() {
		unpauseBtn = GetComponent<Button>();
	}

	// Update is called once per frame
	void Update() {
		if (prevUnpause != GameManager.CanUnpause) {
			timer = 0;
		}

		Vector3 startPos = GameManager.CanUnpause ? start.position : pausePos.position;
		Vector3 endPos = GameManager.CanUnpause ? pausePos.position : end.position;

		EasingFunctions.Function function = GameManager.CanUnpause ? enterFunction : exitFunction;

		/* ---------------------------------
		
				Easing code goes here

		--------------------------------- */

		// Replace this with the lerp function
		transform.position = endPos;
		// -------------------------------------------

		prevUnpause = GameManager.CanUnpause;
	}
}
