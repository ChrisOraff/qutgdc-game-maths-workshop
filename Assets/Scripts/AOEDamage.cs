﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AOEDamage : MonoBehaviour {

    public float damage = 100;

    public float size = 15;

    public List<GameObject> enemies = new List<GameObject>();

    public float lifeTime = 0.1f;

    // Use this for initialization
    void Start () {
        transform.localScale = new Vector3(size,size,size);
        lifeTime = Time.time + lifeTime;
	}
	
	// Update is called once per frame
	void Update () {

        //AOE expiry - damage targets
		if(Time.time > lifeTime) {
            foreach (GameObject enemy in enemies) {
                if (enemy != null) {
                    float thisDamage = (size - Vector3.Distance(enemy.transform.position, transform.position)) * damage;
                    enemy.GetComponent<Enemy>().takeDamage(thisDamage);
                }
            }

            Destroy(this.gameObject);
        }
	}

    void OnTriggerEnter(Collider otherObject) {
        //Add enemies to enemy list
        if (otherObject.tag == "Enemy") {
            enemies.Add(otherObject.gameObject);
        }
    }
}
