﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPauseMuter : MonoBehaviour {

	private AudioSource audioSource;

	private bool prevPaused;

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		if (prevPaused != GameManager.Paused) {
			if (GameManager.Paused) {
				audioSource.Pause();
			}
			else {
				audioSource.UnPause();
			}
		}

		prevPaused = GameManager.Paused;
	}
}
